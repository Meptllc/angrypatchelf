import click
import os
import subprocess


class ExecuteableInformation:
    def __init__(self, file):
        self.file = file
        self.is_32_bit = os.system(f"file {file} | grep 'ELF 32-bit'") == 0
        self.rpath = self.get_current_rpath()


    def fix_dependencies(self, ignore_lib):
        for dep in self.missing_deps():
            self.fix_dep(dep, ignore_lib)


    def get_current_rpath(self):
        return self._run_cmd(f'patchelf --print-rpath {self.file}')


    def fix_interpreter(self):
        if self.is_32_bit:
            raise Exception('32-bit not supported yet')
        interpreter = self._run_cmd(f'patchelf --print-interpreter $(which cp)')
        self._run_cmd(f'patchelf --set-interpreter {interpreter} {self.file}')


    def missing_deps(self):
        return self._run_cmd('ldd ' + self.file + " | grep '=> not found' | awk '{print $1}'").split()


    def deps(self):
        return self._run_cmd('ldd ' + self.file + " | grep '=>' | awk '{print $1}'").split()


    def fix_dep(self, dep, ignore_lib):
        print(f'Fixing: {dep}')
        old_rpath = self.rpath
        for lib in self._run_cmd("nix-locate --whole-name --top-level " + dep + " | awk '{print $4}'").split():
            if ignore_lib and ignore_lib in lib:
                print(f'Ignoring: {lib}')
                continue
            print(f'Trying: {lib}')
            if self.__lib_is_valid(dep, lib, old_rpath):
                print(f'Using: {lib}')
                self.rpath = self.get_current_rpath()
                return

        for lib in self._run_cmd(f"fd --fixed-strings {dep} /nix/store").split():
            print(f'Trying: {lib}')
            if self.__lib_is_valid(dep, lib, old_rpath):
                print(f'Using: {lib}')
                self.rpath = self.get_current_rpath()
                return

        print(f'No useable dependency for {dep}. Perhaps update nix-index?')


    def clear_rpath(self):
        self._run_cmd(f'patchelf --remove-rpath {self.file}')


    def append_rpath(self, lib, old_rpath):
        if old_rpath != '':
            new_rpath = f'{old_rpath}:{os.path.dirname(lib)}'
        else:
            new_rpath = os.path.dirname(lib)
        self._run_cmd(f'patchelf --set-rpath {new_rpath} {self.file}')


    def __lib_is_valid(self, dep, lib, old_rpath):
        lib_is_32_bit = self._run_cmd_is_successful(f"file -L {lib} | grep 'ELF 32-bit'")
        if self.is_32_bit != lib_is_32_bit:
            return False

        self.append_rpath(lib, old_rpath)
        return not self._run_cmd_is_successful('ldd ' + self.file + " | grep '=> not found' | grep '" + dep + "'")


    def _run_cmd_is_successful(self, cmd):
        try:
            subprocess.run(cmd,
                           stdout=subprocess.DEVNULL,
                           stderr=subprocess.DEVNULL,
                           shell=True,
                           check=True)
        except subprocess.CalledProcessError:
            return False
        return True


    def _run_cmd(self, cmd):
        return os.popen(cmd).read().strip()

EXAMPLE_OVERRIDE='librt.so.1:/nix/store/xj7n8ipnlsq9xqv85xlyb2nwis30d01z-glibc-2.34-115/lib/librt.so.1'

@click.command()
@click.argument('elf_file', type=click.Path(exists=True))
@click.option('--overwrite-rpath', is_flag=True, default=False)
@click.option('--ignore-lib', help='Ignore lib paths that contain this string e.g. for glibc-2.33')
def main(elf_file, overwrite_rpath, ignore_lib):
    """Automatically fix an ELF executable."""
    file = ExecuteableInformation(elf_file)
    if overwrite_rpath:
        file.clear_rpath()
    file.fix_interpreter()
    file.fix_dependencies(ignore_lib)
